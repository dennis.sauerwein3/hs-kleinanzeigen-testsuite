package de.hs.da.hskleinanzeigen;

import io.restassured.RestAssured;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.UUID;

import static de.hs.da.hskleinanzeigen.TestUtil.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

@SpringJUnitConfig
@SpringBootTest
@Transactional
class Praktikum5Test {


    private final ArrayList<String> expectedTables = new ArrayList<>() {
        {
            add("AD");
            add("CATEGORY");
            add("USER");
            add("NOTEPAD");
        }
    };

    @Autowired
    private DataSource dataSource;

    private Connection connection;

    private boolean initialized = false;

    @BeforeEach
    void setUp() throws Exception {
        connection = dataSource.getConnection();
        Assertions.assertNotNull(connection);

        RestAssured.baseURI = HOST;
        RestAssured.port = PORT;

        if (TestUtil.isSecurityEnabled()) {
            TestUtil.setAuthenticationForUser();
        } else {
            TestUtil.setNoAuthentication();
        }

        prepareData();
    }

    private void prepareData() throws SQLException {
        if (!initialized) {
            TestUtil.insertData(connection, expectedTables);
            initialized = true;
        }
    }

    @AfterEach
    void tearDown() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }


    /**
     * checks if the endpoint for creating a user after adding the checks for email and name validation still returns http.201 (created)
     * and the returned user doesn't contain the password.
     */
    @Test
    void task2_createUserStatus201() {
        User user = new User();

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CREATED.value())
                .body("$", hasKey("id"))
                .body("$", hasKey("email"))
                .body("$", hasKey("firstName"))
                .body("$", hasKey("lastName"))
                .body("$", hasKey("phone"))
                .body("$", hasKey("location"))
                .body("$", not(hasKey("password")));
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task2_createUserBadEmailStatus400() {
        User user = new User();
        user.email = "invalidEmail";

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task2_createUserNoEmailStatus400() {
        User user = new User();
        user.email = null;

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task2_createUserPasswordToShortStatus400() {
        User user = new User();
        user.password = "kurz";

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task2_createUserNoPasswordStatus400() {
        User user = new User();
        user.password = null;

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task2_createUserNoFirstNameStatus400() {
        User user = new User();
        user.firstName = null;

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task2_createUserFirstNameTooLongStatus400() {
        User user = new User();
        user.firstName = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. " +
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. " +
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec.";

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task2_createUserNoLastNameStatus400() {
        User user = new User();
        user.lastName = null;

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task2_createUserLastNameTooLongStatus400() {
        User user = new User();
        user.lastName = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. " +
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. " +
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec.";

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the application generates at least 1000 test Users
     *
     * @throws SQLException
     */
    @Test
    void task3_check1000TestUserExists() throws SQLException {
        int numberOfUsers;
        final String countUsers = "SELECT COUNT(DISTINCT EMAIL) AS ANZAHL FROM USER";
        try (Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(countUsers);
            rs.next();
            numberOfUsers = rs.getInt("ANZAHL");
        }

        Assertions.assertTrue(numberOfUsers >= 1000, "Not enough test Data in Database. Only " + numberOfUsers + "exist");
    }

    /**
     * checks if requesting a User page returns a page and http.200 (OK)
     */
    @Test
    void task4_getUserPageStatus200() {
        given()
                .basePath(BASE_PATH_USER)
                .accept("application/json")
                .pathParams("pageStart", 0, "pageSize", 1)
                .when().get("?pageStart={pageStart}&pageSize={pageSize}")
                .then().statusCode(HttpStatus.OK.value())
                .body("content", notNullValue())
                .body("pageable", notNullValue())
                .body("totalPages", notNullValue())
                .body("totalElements", notNullValue());
    }

    /**
     * Tests the response if the requested page does not exist.
     * Be aware that this tests assumes that there are less than 100.000 users in database.
     */
    @Test
    void task4_getUserPageStatus204() {
        given()
                .basePath(BASE_PATH_USER)
                .accept("application/json")
                .pathParams("pageStart", 100000, "pageSize", 10)
                .when().get("?pageStart={pageStart}&pageSize={pageSize}")
                .then().statusCode(HttpStatus.NO_CONTENT.value());
    }

    /**
     * checks if requesting an invalid User page returns a page and http.400 (Bad request)
     */
    @Test
    void task4_getUserPageInvalidPageSizeStatus400() {
        given()
                .basePath(BASE_PATH_USER)
                .accept("application/json")
                .pathParams("pageStart", 0, "pageSize", -1)
                .when().get("?pageStart={pageStart}&pageSize={pageSize}")
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if requesting an invalid User page returns a page and http.400 (Bad request)
     */
    @Test
    void task4_getUserPageInvalidPageStartStatus400() {
        given()
                .basePath(BASE_PATH_USER)
                .accept("application/json")
                .pathParams("pageStart", -1, "pageSize", 0)
                .when().get("?pageStart={pageStart}&pageSize={pageSize}")
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if requesting an invalid User page returns a page and http.400 (Bad request)
     */
    @Test
    void task4_getUserPageInvalidPageStartAndSizeStatus400() {
        given()
                .basePath(BASE_PATH_USER)
                .accept("application/json")
                .pathParams("pageStart", -1, "pageSize", -1)
                .when().get("?pageStart={pageStart}&pageSize={pageSize}")
                .then().statusCode(HttpStatus.BAD_REQUEST.value());

    }

    class User {
        String email = UUID.randomUUID().toString() + "@mynewdomain.de";
        String password = "secret";
        String firstName = "my first name";
        String lastName = "my second name";
        String phone = "0176822222222";
        String location = "Darmstadt";

        String toJson() {
            String json = "{\n";
            if (email != null) {
                json += "    \"email\": \"" + email + "\",\n";

            }
            if (password != null) {
                json += "    \"password\": \"" + password + "\",\n";

            }

            if (firstName != null) {


                json += "    \"firstName\": \"" + firstName + "\",\n";
            }

            if (lastName != null) {
                json += "    \"lastName\": \"" + lastName + "\",\n";

            }

            json += "    \"phone\": \"" + phone + "\",\n" +
                    "    \"location\": \"" + location + "\"\n" +
                    "}";
            return json;
        }
    }
}
