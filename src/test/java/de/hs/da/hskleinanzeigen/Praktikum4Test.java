package de.hs.da.hskleinanzeigen;

import io.restassured.RestAssured;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

@SpringJUnitConfig
@SpringBootTest
@Transactional
class Praktikum4Test {

    private static final String email = "studi@hs-da.de";
    private static final String payload = "{\n" +
            "   \"email\":\"" + email + "\",\n" +
            "   \"password\":\"secret\",\n" +
            "   \"firstName\":\"Thomas\",\n" +
            "   \"lastName\":\"Müller\",\n" +
            "   \"phone\":\"069-123456\",\n" +
            "   \"location\":\"Darmstadt\"\n" +
            "}\n";
    private static final String payload_incomplete = "{\n" +
            "   \"password\":\"secret\",\n" +
            "   \"firstName\":\"Thomas\",\n" +
            "   \"lastName\":\"Müller\",\n" +
            "   \"phone\":\"069-123456\",\n" +
            "   \"location\":\"Darmstadt\"\n" +
            "}\n";

    private static final String categoryName = "Nachhilfe";
    private static final String category_payload = "{\n" +
            "   \"parentId\":\"4711\",\n" +
            "   \"name\":\"" + categoryName + "\"\n" +
            "}\n";
    private static final String category_payload_incomplete = "{\n" +
            "   \"parentId\":\"4711\",\n" +
            "}\n";

    private static final String notepadAdId = "1";
    private static final String notepad_payload = "{\n" +
            "   \"advertisementId\":" + notepadAdId + ",\n" +
            "   \"note\":\"Zimmer direkt bei der HS\"\n" +
            "}\n";
    private static final String notepad_payload_incomplete = "{\n" +
            "   \"note\":\"Zimmer direkt bei der HS\"\n" +
            "}\n";
    private final ArrayList<String> expectedTables = new ArrayList<>() {
        {
            add("AD");
            add("CATEGORY");
            add("USER");
            add("NOTEPAD");
        }
    };

    @Autowired
    private DataSource dataSource;

    private Connection connection;

    private boolean initialized = false;

    @BeforeEach
    void setUp() throws Exception {
        connection = dataSource.getConnection();
        Assertions.assertNotNull(connection);

        RestAssured.baseURI = TestUtil.HOST;
        RestAssured.port = TestUtil.PORT;

        if (TestUtil.isSecurityEnabled()) {
            TestUtil.setAuthenticationForUser();
        } else {
            TestUtil.setNoAuthentication();
        }

        prepareData();
    }

    private void prepareData() throws SQLException {
        if (!initialized) {
            TestUtil.insertData(connection, expectedTables);
            initialized = true;
        }
    }

    @AfterEach
    void tearDown() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    /**
     * checks if the tables exist
     * Notepad is new
     *
     * @throws SQLException if the connection failed
     */
    @Test
    void task1_newNotepadTableExists() throws SQLException {
        ArrayList<String> existingTables = new ArrayList<>();

        DatabaseMetaData md = connection.getMetaData();
        ResultSet rs = md.getTables(null, null, "%", null);
        while (rs.next()) {
            if (expectedTables.contains(rs.getString("TABLE_NAME"))) {
                existingTables.add(rs.getString("TABLE_NAME"));
            }
        }

        Assertions.assertEquals(expectedTables.size(), existingTables.size());
    }

    /**
     * expected table layout:
     * ID	Integer, Primary Key, auto increment
     * USER_ID	Integer, not null, Foreign Key auf USER.ID
     * AD_ID	Integer, not null, Foreign Key auf AD.ID
     * NOTE	Varchar
     * CREATED	Timestamp, not null
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task1_checkNotepadColumnsExist() throws SQLException {
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "NOTEPAD", "ID", TestUtil.FieldType.INTEGER), "Column ID is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "NOTEPAD", "USER_ID", TestUtil.FieldType.INTEGER), "Column USER_ID is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "NOTEPAD", "AD_ID", TestUtil.FieldType.INTEGER), "Column AD_ID is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "NOTEPAD", "NOTE", TestUtil.FieldType.VARCHAR), "Column NOTE is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "NOTEPAD", "CREATED", TestUtil.FieldType.TIMESTAMP), "Column CREATED is missing or has invalid data type");
    }

    /**
     * checks if inserting users works
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task2_insertUserStatus200() throws SQLException {
        deleteUserInDatabase();

        given()
                .basePath(TestUtil.BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(payload)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CREATED.value())
                .body("id", notNullValue())
                .body("email", equalTo(email))
                .body("firstName", equalTo("Thomas"))
                .body("lastName", equalTo("Müller"))
                .body("phone", equalTo("069-123456"))
                .body("location", equalTo("Darmstadt"))
                .body("$", not(hasKey("password")));


        final String query = "select * from USER\n" +
                "where EMAIL = \"" + email + "\"";
        try (Statement statement = connection.createStatement()) {
            ResultSet result = statement.executeQuery(query);
            Assertions.assertTrue(result.next());
        }
    }

    /**
     * checks if inserting an invalid user returns Http.400 (Bad Request)
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task2_insertUserStatus400() throws SQLException {
        deleteUserInDatabase();

        given()
                .basePath(TestUtil.BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(payload_incomplete)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if inserting a user with the same email twice returns Http.409 (Conflict)
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task2_insertUserStatus409() throws SQLException {
        deleteUserInDatabase();

        // add user for the first time
        given()
                .basePath(TestUtil.BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(payload)
                .accept("application/json")
                .when().post().then().statusCode(HttpStatus.CREATED.value());

        // add user for the second time
        given()
                .basePath(TestUtil.BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(payload)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CONFLICT.value());
    }

    /**
     * checks if creating a category works
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task3_createCategoryStatus200() throws SQLException {
        deleteCategoryInDatabase();

        given()
                .basePath(TestUtil.BASE_PATH_CATEGORY)
                .header("Content-Type", "application/json")
                .body(category_payload)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CREATED.value())
                .body("id", notNullValue())
                .body("name", equalTo(categoryName));

        final String query = "select * from CATEGORY\n" +
                "where NAME = \"" + categoryName + "\"";
        try (Statement statement = connection.createStatement()) {
            ResultSet result = statement.executeQuery(query);
            Assertions.assertTrue(result.next());
        }
    }

    /**
     * checks if creating an invalid category returns Http.400 (Bad Request)
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task3_createCategoryStatus400() throws SQLException {
        deleteCategoryInDatabase();

        given()
                .basePath(TestUtil.BASE_PATH_CATEGORY)
                .header("Content-Type", "application/json")
                .body(category_payload_incomplete)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());

    }

    /**
     * checks if creating a category twice returns Http.409 (Conflict)
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task3_createCategoryStatus409() throws SQLException {
        deleteCategoryInDatabase();

        // add category for the first time
        given()
                .basePath(TestUtil.BASE_PATH_CATEGORY)
                .header("Content-Type", "application/json")
                .body(category_payload)
                .accept("application/json")
                .when().post().then().statusCode(HttpStatus.CREATED.value());

        // add category for the second time
        given()
                .basePath(TestUtil.BASE_PATH_CATEGORY)
                .header("Content-Type", "application/json")
                .body(category_payload)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CONFLICT.value());
    }

    /**
     * checks if http get on the notepad of a user returns http.200 (OK)
     */
    @Test
    void task4_getNotepadStatus200() {
        given().log().all()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .pathParam("userId", 1)
                .accept("application/json")
                .when().get()
                .then().statusCode(HttpStatus.OK.value())
                .body("size()", greaterThanOrEqualTo(1))
                .body("[0].id", notNullValue())
                .body("[0].advertisement", notNullValue())
                .body("[0].advertisement.id", allOf(instanceOf(Integer.class), notNullValue()))
                .body("[0].advertisement.type", allOf(instanceOf(String.class), notNullValue()))
                .body("[0].advertisement.title", allOf(instanceOf(String.class), notNullValue()))
                .body("[0].advertisement.price", allOf(instanceOf(Integer.class), notNullValue()))
                .body("[0].advertisement.location", allOf(instanceOf(String.class), notNullValue()))
                .body("[0].note", allOf(instanceOf(String.class), notNullValue()));
    }

    /**
     * checks if requesting the notepad of a not existing user returns http.04 (Not found)
     */
    @Test
    void task4_getNotepadStatus404() {
        given()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .pathParam("userId", -1)
                .accept("application/json")
                .when().get()
                .then().statusCode(HttpStatus.NOT_FOUND.value());
    }

    /**
     * checks if creating a notepad returns http.201 (created)
     * and the notepad is persisted in the database
     *
     * @throws SQLException
     */
    @Test
    void task5_createNotepadStatus201() throws SQLException {
        // TODO maybe we can delete this when POST of notepads is refactored
        final String deleteNotepadStatement = "DELETE FROM NOTEPAD\n" +
                "WHERE AD_ID = " + notepadAdId;
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(deleteNotepadStatement);
        }

        given()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .header("Content-Type", "application/json")
                .pathParam("userId", 1)
                .body(notepad_payload)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CREATED.value())
                .body("id", notNullValue())
                .body("advertisementId", notNullValue())
                .body("note", equalTo("Zimmer direkt bei der HS"));

        final String query = "select * from NOTEPAD\n" +
                "where AD_ID = \"" + notepadAdId + "\" and USER_ID = 1";
        try (Statement statement = connection.createStatement()) {
            ResultSet result = statement.executeQuery(query);
            Assertions.assertTrue(result.next(), "No notepad found in Database for the User ID / 1 and Notepad ID / " + notepadAdId + ".");
        }
    }

    /**
     * checks if posting an invalid notepad returns http.400 (Bad REQUEST)
     */
    @Test
    void task5_createNotepadStatus400() {
        given()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .header("Content-Type", "application/json")
                .pathParam("userId", 1)
                .body(notepad_payload_incomplete)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * TODO POST always creates new notepad, add PUT for update (on concrete notepadId)
     *
     * checks if updating a notepad returns htpp.201 (created)
     * -> no http.409 (conflict)
     */
    @Test
    @Disabled
    void task5_updateNotepad201() {
        // add notepad for the first time
        given()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .header("Content-Type", "application/json")
                .pathParam("userId", 1)
                .body(notepad_payload)
                .accept("application/json")
                .when().post().then().statusCode(HttpStatus.CREATED.value());

        // add notepad for the second time
        given()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .header("Content-Type", "application/json")
                .pathParam("userId", 1)
                .body(notepad_payload)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CREATED.value());
    }

    /**
     * checks if deleting a notepad returns http.204 (no content)
     * and the notepad no longer exists in database
     *
     * @throws SQLException
     */
    @Test
    void task6_deleteNotepadStatus204() throws SQLException {
        given()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .header("Content-Type", "application/json")
                .pathParams("userId", 1, "adID", notepadAdId)
                .when().delete("{adID}")
                .then().statusCode(HttpStatus.NO_CONTENT.value());

        int numberOfNotepads;
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT COUNT(ID) AS ANZAHL FROM NOTEPAD WHERE USER_ID = 1 AND AD_ID = " + notepadAdId);
            resultSet.next();
            numberOfNotepads = resultSet.getInt("ANZAHL");
        }
        Assertions.assertEquals(0, numberOfNotepads, "The notepad for user ID /1 and AD ID /" + notepadAdId + " still exists.");

    }


    /**
     * checks if the requesting an ad matches the new specifications of task7
     */
    @Test
    void task7_getAdResult200() {
        given()
                .basePath(TestUtil.BASE_PATH_AD)
                .pathParam("id", 1)
                .accept("application/json")
                .when().get("{id}")
                .then().statusCode(HttpStatus.OK.value())
                .body("id", notNullValue())
                .body("type", equalTo("Offer"))
                .body("category", notNullValue())
                .body("title", equalTo("Titel"))
                .body("description", equalTo("Beschreibung"))
                .body("price", equalTo(42))
                .body("location", equalTo("Standort"));

    }

    @Test
    void task7_getAdResult400() {
        given()
                .accept("application/json")
                .basePath(TestUtil.BASE_PATH_AD)
                .pathParam("id", -1)
                .when().get("{id}")
                .then().statusCode(HttpStatus.NOT_FOUND.value());

    }

    private void deleteUserInDatabase() throws SQLException {
        final String deleteUser = "DELETE FROM USER\n" +
                "WHERE EMAIL = \"" + email + "\"";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate("SET SQL_SAFE_UPDATES = 0");
            statement.executeUpdate(deleteUser);
        }
    }

    private void deleteCategoryInDatabase() throws SQLException {
        final String deleteCategory = "DELETE FROM CATEGORY\n" +
                "WHERE NAME = \"" + categoryName + "\"";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate("SET SQL_SAFE_UPDATES = 0");
            statement.executeUpdate(deleteCategory);
        }
    }
}
