package de.hs.da.hskleinanzeigen;

import static org.assertj.core.api.Assertions.assertThat;

import io.restassured.RestAssured;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@SpringJUnitConfig
@SpringBootTest
@Transactional
class Praktikum3Test {

    private final ArrayList<String> expectedTables = new ArrayList<>() {
        {
            add("AD");
            add("CATEGORY");
            add("USER");
        }
    };

    @Autowired
    private DataSource dataSource;

    private Connection connection;

    @BeforeEach
    void setUp() throws Exception {
        connection = dataSource.getConnection();
        Assertions.assertNotNull(connection);

        RestAssured.baseURI = TestUtil.HOST;
        RestAssured.port = TestUtil.PORT;

        if (TestUtil.isSecurityEnabled()) {
            TestUtil.setAuthenticationForUser();
        } else {
            TestUtil.setNoAuthentication();
        }
    }

    @AfterEach
    void tearDown() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    @Test
    void task3_liquibaseTablesExists() throws SQLException {
        Set<String> existingTables = new HashSet<>();
        DatabaseMetaData md = connection.getMetaData();
        ResultSet rs = md.getTables(null, null, "%", null);
        while ( rs.next() ) {
            existingTables.add(rs.getString("TABLE_NAME").toUpperCase());
        }

        assertThat(existingTables).contains("DATABASECHANGELOG").withFailMessage("Liquibase meta data table 'DATABASECHANGELOG' not found in database");
        assertThat(existingTables).contains("DATABASECHANGELOGLOCK").withFailMessage("Liquibase meta data table 'DATABASECHANGELOGLOCK' not found in database");

        assertThat(existingTables).contains("CATEGORY").withFailMessage("table 'CATEGORY' not found in database");
        assertThat(existingTables).contains("AD").withFailMessage("table 'AD' not found in database");
    }

    /**
     * checks if the application inserts enough advertisements
     *
     * @throws SQLException if the connection failed
     */
    @Test
    void task4_checkIfADTestdataExists() throws SQLException {
        final String countAds = "SELECT COUNT(ID) AS ANZAHL FROM AD";
        try (Statement statement = connection.createStatement()) {
            int numberOfAds;

            try (ResultSet rs = statement.executeQuery(countAds)) {
                rs.next();
                numberOfAds = rs.getInt("ANZAHL");
            }

            Assertions.assertTrue(numberOfAds >= 5);
        }
    }

    /**
     * checks if the application inserts enough categegories
     *
     * @throws SQLException if the connection failed
     */
    @Test
    void task4_checkIfCategoryTestdataExists() throws SQLException {
        final String countCategories = "SELECT COUNT(ID) AS ANZAHL FROM CATEGORY ";
        try (Statement statement = connection.createStatement()) {
            int numberOfCategories;
            try (ResultSet rs = statement.executeQuery(countCategories)) {
                rs.next();
                numberOfCategories = rs.getInt("ANZAHL");
            }

            Assertions.assertTrue(numberOfCategories >= 3);
        }
    }


    /**
     * checks if the expected tables exists
     * User table is new
     *
     * @throws SQLException if the connection failed or the database is badly configured
     */
    @Test
    void task5_newUserTableExists() throws SQLException {
        boolean hasUser = false;
        List<String> existingTables = new ArrayList<>();

        DatabaseMetaData md = connection.getMetaData();
        ResultSet rs = md.getTables(null, null, "%", null);
        while (rs.next()) {
            if (expectedTables.contains(rs.getString("TABLE_NAME"))) {
                existingTables.add(rs.getString("TABLE_NAME"));
            }
        }

        Assertions.assertEquals(expectedTables.size(), existingTables.size());

        ResultSet columns = connection.getMetaData().getColumns(null, null, "AD", null);
        while (columns.next()) {
            if (columns.getString("COLUMN_NAME").equals("USER_ID")) {
                hasUser = true;
                break;
            }
        }
        Assertions.assertTrue(hasUser);
    }

    /**
     * expected table layout:
     * ID	Integer, Primary Key, auto increment
     * EMAIL	Varchar, not null
     * PASSWORD	Varchar, not null
     * FIRST_NAME	Varchar
     * LAST_NAME	Varchar
     * PHONE	Varchar
     * LOCATION	Varchar
     * CREATED	Timestamp, not null
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task5_checkUserColumnsExist() throws SQLException {
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "USER", "ID", TestUtil.FieldType.INTEGER), "Column ID is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "USER", "EMAIL", TestUtil.FieldType.VARCHAR), "Column EMAIL is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "USER", "PASSWORD", TestUtil.FieldType.VARCHAR), "Column PASSWORD is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "USER", "FIRST_NAME", TestUtil.FieldType.VARCHAR), "Column FIRST_NAME is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "USER", "LAST_NAME", TestUtil.FieldType.VARCHAR), "Column LAST_NAME is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "USER", "PHONE", TestUtil.FieldType.VARCHAR), "Column PHONE is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "USER", "LOCATION", TestUtil.FieldType.VARCHAR), "Column LOCATION is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "USER", "CREATED", TestUtil.FieldType.TIMESTAMP), "Column CREATED is missing or has invalid data type");
    }
}
