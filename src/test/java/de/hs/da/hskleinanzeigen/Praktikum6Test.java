package de.hs.da.hskleinanzeigen;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import static io.restassured.RestAssured.given;

@SpringJUnitConfig
@SpringBootTest
@Transactional
class Praktikum6Test {


    @BeforeEach
    void setUp() {
        RestAssured.baseURI = TestUtil.HOST;
        RestAssured.port = TestUtil.PORT;

        TestUtil.setNoAuthentication();
    }

    /**
     * checks allowed enpoints for user WITHOUT authentication
     */
    @Test
    void task1_allowedEndpointsNoCredentials() {
        given().when().get("/hs-kleinanzeigen/actuator/info")//
                .then().statusCode(HttpStatus.OK.value());

        given().when().get("/hs-kleinanzeigen/actuator/health")//
                .then().statusCode(HttpStatus.OK.value());
    }

    /**
     * checks forbidden enpoints for user WITHOUT authentication
     */
    @Test
    void task1_forbiddenEndpointsNoCredentials() {

        given().when().get("/hs-kleinanzeigen/api/users/1")//
                .then().statusCode(HttpStatus.UNAUTHORIZED.value());

        given().when().get("/hs-kleinanzeigen/actuator/metrics")//
                .then().statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    /**
     * checks forbidden enpoints for user with INVALID authentication
     */
    @Test
    void task1_forbiddenEndpointsInvalidCredentials() {
        TestUtil.setAuthentication("user", "asdadfjhglkdfgfg");

        given().when().get("/hs-kleinanzeigen/actuator/info")//
                .then().statusCode(HttpStatus.UNAUTHORIZED.value());

        given().when().get("/hs-kleinanzeigen/actuator/health")//
                .then().statusCode(HttpStatus.UNAUTHORIZED.value());

        given().when().get("/hs-kleinanzeigen/api/users/1")//
                .then().statusCode(HttpStatus.UNAUTHORIZED.value());

        given().when().get("/hs-kleinanzeigen/actuator/metrics")//
                .then().statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    /**
     * checks allowed enpoints for user with USER authentication
     */
    @Test
    void task1_allowedEndpointsUserCredentials() {
        TestUtil.setAuthenticationForUser();

        given().when().get("/hs-kleinanzeigen/actuator/info")//
                .then().statusCode(HttpStatus.OK.value());

        given().when().get("/hs-kleinanzeigen/actuator/health")//
                .then().statusCode(HttpStatus.OK.value());

        given().when().get("/hs-kleinanzeigen/api/users/1")//
                .then().statusCode(HttpStatus.OK.value());
    }

    /**
     * checks forbidden enpoints for user with USER authentication
     */
    @Test
    void task1_forbiddenEndpointsUserCredentials() {
        TestUtil.setAuthenticationForUser();

        given().when().get("/hs-kleinanzeigen/actuator/metrics")//
                .then().statusCode(HttpStatus.FORBIDDEN.value());
    }

    /**
     * checks allowed enpoints for user with ADMIN authentication
     */
    @Test
    void task1_allowedEndpointsAdminCredentials() {
        TestUtil.setAuthentication("admin", "admin");

        given().when().get("/hs-kleinanzeigen/actuator/info")//
                .then().statusCode(HttpStatus.OK.value());

        given().when().get("/hs-kleinanzeigen/actuator/health")//
                .then().statusCode(HttpStatus.OK.value());

        given().when().get("/hs-kleinanzeigen/api/users/1")//
                .then().statusCode(HttpStatus.OK.value());

        given().when().get("/hs-kleinanzeigen/actuator/metrics")//
                .then().statusCode(HttpStatus.OK.value());
    }
}
